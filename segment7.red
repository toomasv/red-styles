Red[]

segment7!: context  [
;
;           size:		number size
;  --   1
; |  | 2 3  soff:		short-offset - offset from line to parallel border
;  --   4   middle:		position of center line
; |  | 5 6  loff:		long-offset - offset from line start/end to border
;  --   7
;			spacing:	space between numbers
;
	soff: 3
	loff: 5
	spacing: 5
	line-width: 5
	color: 126.161.165
	size: 40x80
	middle: size/y / 2
	segments: [
		[line (as-pair loff soff) (as-pair size/x - loff soff)]
		[line (as-pair soff loff) (as-pair soff middle - soff)]
		[line (as-pair size/x - soff loff) (as-pair size/x - soff middle - soff)]
		[line (as-pair loff middle) (as-pair size/x - loff middle)]
		[line (as-pair soff middle + soff) (as-pair soff size/y - loff)]
		[line (as-pair size/x - soff middle + soff) (as-pair size/x - soff size/y - loff)]
		[line (as-pair loff size/y - soff) (as-pair size/x - loff size/y - soff)]
	]
	numbers: [
		#"0" [1 2 3 5 6 7]
		#"1" [3 6]
		#"2" [1 3 4 5 7]
		#"3" [1 3 4 6 7]
		#"4" [2 3 4 6]
		#"5" [1 2 4 6 7]
		#"6" [1 2 4 5 6 7]
		#"7" [1 3 6]
		#"8" [1 2 3 4 5 6 7]
		#"9" [1 2 3 4 6 7]
	]
	make-number: func [number /local segs][
		segs: select numbers number
		collect [
			foreach number segs [
				keep compose segments/:number
			]
		]
	]
	draw: func [
		text
		/local canvas
	][
		text: form text
		canvas: copy []
		offset: 0
		foreach number text [
			append canvas compose/deep [
				translate (as-pair offset 0) [(make-number number)]
			]
			offset: offset + size/x + spacing ; TODO: add spacing as settings
		]
		compose/deep [
			line-width (line-width)
			line-cap round
			pen (color)
			skew -3 [(canvas)]
		]
	]
]
